//##SON CALISAN##

#include <Wire.h>              //serial baglantinin kutuphanesi
#include <SoftwareSerial.h>    //bluetooth bağlantısı için master/slave methodu için
SoftwareSerial BModule(10, 11); // RX | TX bağlantısı için 10 ve 11. pinler kullanıldı

String inputString="";  //serialdan okunan veriyi string değişkenine dönüştürmekte

struct İnfo{
  char *harf[];
  int *sayı[];
}


void setup() {
  Wire.begin(8);                // i2c testine gore 8  bitlik iletisim kurduk.
  Wire.onReceive(masterbilgialma); // Seri baglanti uzerinden fonksiyonun calisir hale gelmesi icin Wire kutuphanesini kullanarak receiveEvent fonksiyonunu cagirdik.
  BModule.begin(9600);           //Bluetooth için 9600 bandında seri bağlantı oluşturmaktadır.
  Serial.println("Bluetooth açık!!! '1234' parolasıyla bağlanabilirsiniz."); //serial monitörde bağlantının başladığını göstermektedir.
  Serial.begin(9600);           //seri bağlantıyı 9600 bandında başlatıyor

}

void loop() {
  delay(400);                      // her olay arasinda gecmesi gereken sure
  if (BModule.available())          //ulaşılabilen bluetooth bağlantı kontrolü
  {
	Serial.write(BModule.read());   //bluetooth dan okunan veriyi serial monitöre yazdırmaktadır
}
  if (Serial.available()){        //serial bağlatıda ulaşılabilirlik kontrolü
     BModule.write(Serial.read());  //serialdan okunan veriyi bluetooth monitörüne yazdırmakta
  }

  int sensorValueA3=analogRead(A0);
  int sensorValueA4=analogRead(A1);
  slavesensor(sensorValueA3,sensorValueA4);

}

void masterbilgialma(int baglantısayısı) {    // Seri baglantida ve bluetooth bağlantısında kullanilacak olan fonksiyon tanimi

  struct İnfo birlesik;
  char c[]={a};
  while (1 < Wire.available()) {  // En az bir baglantinin gerceklesip gerceklesmedigi kontrol edilir.
    char z = Wire.read();
    for(int i=0;i<strlen(c);i++){
      c[i] = Wire.read();
    }
  //   char c = Wire.read();         // Baglanti uzerinden okunabilen veriyi c adinda bir char veri tipine cevirmektedir.
  //   for(int i=0;i<strlen(birlesik.harf);i++){
  //   birlesik.harf[i] = c;
  // }
    Serial.print(c);
    BModule.print(c);              // c değişkenini bluetooth modülüne yazmakta
  }
  int s[]={0};
  for(int l = 0; l< strlen(s);l++){
    s[l]=Wire.read();
  }
  //int x = Wire.read();    // Diger makinenin gonderegi veriye gore degismektedir ama burada int kullandik. Gelen bitlerin toplanip x degiskenine atanmaktadir.
  Serial.println(x);
  BModule.println(x);      // x değişkenin bluetooth montörüne bastırmaktadır

  BModule.print("\n");
  BModule.print(A0);
  BModule.print("\n");
  BModule.print(A1);
  BModule.print("\n");
  BModule.print(A2);
  BModule.print("\n");
  BModule.print("A3 : ");
  BModule.println(A3);                              // bluetooth moitörüne yazdırmaktadır.
  BModule.print("\nA4 : ");
  BModule.println(A4);


}

void slavesensor(int sensorValueA3, int sensorValueA4) {
     sensorValueA3 = map(sensorValueA3,0,1023,0,1080);
     sensorValueA4 = map(sensorValueA4,0,1023,0,1080);
     Serial.print("A3 : ");
     Serial.println(sensorValueA3);
     Serial.print("A4 : ");
     Serial.println(sensorValueA4);
}
