##SON CALISAN EKRAN ICIN##

#include <Wire.h>                           //serial baglantinin kutuphanesi
#include <LiquidCrystal.h>                  // lcd kutuphanesi

const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;   //lcd'nin giris pinleri
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);                    //lcd'nin kutuphane baglantisi.


void seup() {
  Wire.begin(8);                // i2c testine gore 8  bitlik iletisim kurduk.
  Wire.onReceive(receiveEvent); // Seri baglanti uzerinden fonksiyonun calisir hale gelmesi icin Wire kutuphanesini kullanarak receiveEvent fonksiyonunu cagirdik.
  Serial.begin(9600);           // serial baglatinin baslamasi icin 9600 bant genisliginde kullandik.
  lcd.begin(16, 2);             // Lcd'nin ekran boyutunu belirtmekte 16 sutun 2 satir'dan gelmektedir.
}

void loop() {
  delay(100);                   // her olay arasinda gecmesi gereken sure
}

void receiveEvent(int howMany) {      // Seri baglantida kullanilacak olan fonksiyon tanimi
  int sensorValueA3=analogRead(A3);   // Sensor uzerinde gelenler int oldugu icin sensor baglantilari int olarak tanimlandi. A3 ve A4 sensorleri icin gecerlidir.
  int sensorValueA4=analogRead(A4);
  lcd.setCursor(0,0);                 // lcd'nin verileri nerede gosterecegini belirmektedir. 0.sutun ve 0. satir dan baslayarak lcd'de gosterilmesi hedeflenmistir.

  while (1 < Wire.available()) {      // En az bir baglantinin gerceklesip gerceklesmedigi kontrol edilir.
    char c = Wire.read();             // Baglanti uzerinden okunabilen veriyi c adinda bir char veri tipine cevirmektedir.
    Serial.print(c);                  // Serial monitor uzerine yazdirma.
  }
  
  int x = Wire.read();                // Diger makinenin gonderegi veriye gore degismektedir ama burada int kullandik. Gelen bitlerin toplanip x degiskenine atanmaktadir.
  lcd.print(x);                       // lcd uzerine yazdirilmaktadir.
  Serial.println(x);                  // Serial monitore yazdirilmaktadir.

  sensorValueA3 = map(sensorValueA3,0,1023,0,180);    // Derece araliginin ayarlanmasi icin kullanildi. 0 1023 arasinda deger gelmektedir. 0 180 derecesi arasinda gostermesilidir.
  lcd.setCursor(0,1);                                 // lcd satir sutun ayarlamasi
  lcd.print("A3:");                                   // lcd de gosterilecek sensor adi ve degisken tanimlamasi.
  lcd.print(sensorValueA3);
  lcd.print(" ");
  sensorValueA4 = map(sensorValueA4,0,1023,0,180);    // Derece araliginin ayarlanmasi icin kullanildi. 0 1023 arasinda deger gelmektedir. 0 180 derecesi arasinda gostermesilidir.
  Serial.println(A3);                                 // Serial ekranina bastirilmasini saglar.
  Serial.println(A4);
  lcd.print("A4:");                                   // lcd satir sutun ayarlamasi
  lcd.print(sensorValueA4);                           // lcd de gosterilecek sensor adi ve degisken tanimlamasi.
  lcd.print(" ");

}
