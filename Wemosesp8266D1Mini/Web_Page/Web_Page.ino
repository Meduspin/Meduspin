#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>

#include "index.h" //Our HTML webpage contents


//SSID and Password of your WiFi router
const char* ssid = "My ASUS";
const char* password = "testtest";

int sensorPin = 0;
int value = 0;

ESP8266WebServer server(80); //Server on port 80


void handleRoot() {
 String s = MAIN_page; //Read HTML contents

 server.send(200, "text/html", s ); //Send web page
}

void sensorValue(){
   value = analogRead(sensorPin);
 String value1= String(value);
 server.send(200, "text/plane", value1);
  }

void setup(void) {
  Serial.begin(9600);
  WiFi.begin(ssid, password);     //Connect to your WiFi router
  Serial.println("");
  
  // put your setup code here, to run once:
  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
    //If connection successful show IP address in serial monitor
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());  //IP address assigned to your ESP
 
  server.on("/", handleRoot);      //Which routine to handle at root location
   server.on("/read", sensorValue);
 
  server.begin();                  //Start server
  Serial.println("HTTP server started");
}

void loop() {

  // put your main code here, to run repeatedly:
  server.handleClient();          //Handle client requests
}
