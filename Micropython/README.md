# Micropython

Micropython'a gecis.

Micropython yuklemek icin esptools.py yuklemeniz gerekiyor. esptools.py ile wemos d1 miniyi erase memory flash yapmaniz gerekiyor sonrasinda reponun icerisinde  firmware ile baslayan dosyayi yuklemelisiniz.


```
esptool.py --port /dev/ttyUSB0 --baud 460800 write_flash -fm dio --flash_size=detect 0 ~/Downloads/firmware-combined.bin
```

'-fm dio' parametresi wemoslar icin kullanilan memory miktrinin az oldugu yerde kullanilir.