#include <Wire.h>         // Seri baglanti icin kutuphane tanimlamasi


void setup() {
  Wire.begin();           // Seri baglantiyi baslatmaktadir.
  Serial.begin(9600);     // Seri baglantiyi 9600 bandina ayalarmaktadir.
}
void loop() {
  int sensorValueA0=analogRead(A0);                   // A0, A1 ve A2'nin pinlerin ayarlamasi
  int sensorValueA1=analogRead(A1);
  int sensorValueA2=analogRead(A2);
  sensorValueA0 = map(sensorValueA0,0,1023,0,180);    // A0, A1 ve A2'nin 0 1023 degerlerini 0 180 arasinda degerlere donusmektedir.
  sensorValueA1 = map(sensorValueA1,0,1023,0,180);
  sensorValueA2 = map(sensorValueA2,0,1023,0,180);

  delay(700);                                         // 700 ms gecikme olmasini saglar
  Wire.beginTransmission(8);              // serial baglantinin 8 bitlik baglanti uzerinden kurulmasini saglar.
  Wire.write("A0'nin derecesi: ");        // A0, A1 ve A2 sabit ve degiskenleri icin serial baglantida yazdirma islemi yapar. 
  Wire.write(sensorValueA0);
  Wire.endTransmission();                 // Serial baglantida yazdirma isleminin sonu.
  delay(50);
  Wire.beginTransmission(8);
  Wire.write("A1'nin derecesi: ");
  Wire.write(sensorValueA1);
  Wire.endTransmission();
  Wire.beginTransmission(8);
  Wire.write("A2'nin derecesi: ");
  Wire.write(sensorValueA2);
  Wire.endTransmission();   
}
