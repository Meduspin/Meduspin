## Meduspin

Meduspin adında yaptığımız grup projesinin arduino kodları bulunmaktadır. Bu kodları ne işe yaradığını bilmeden kullanmayınız. Malzemelerinize zarar verebilir. Kullanmadan önce proje ekibine danışınız.

Değişiklik yaptıktan sonra lütfen README dosyasına yapılan değişiklikşleri not ediniz.

+ Teknofest_2017: 2017 senesinde katıldığımız Teknofest yarışmasının kodlarıdır. Yarışmada finale kaldık fakat dereceye giremedik.
+ Wemosesp8266D1Mini: Wifi modülüyle ilgili kodlar.


1. Aldığımız wifi modülünü web arayüzene aktarmaktadır. Web arayüzene ışık müktarını ölçen KY-108 modülü kullanılmıştır. 2 sn'de bir sayfaya veri aktarılmaktadır. Wifi modülü usb ile bağlanmış olup programlama onun üzerinden yapılmıştır.  

2. Projenin benim için biten kısmı, Veritabanını oluşturup tekrardan düzenledim. Backup dosyası Converter Beklemesi dizini içindedir. Php kodlarını güncelledim. 5 sensöre uygun şekilde genişletip web sayfasının tablo şekilinde olmasını sağladım.

3. Python kodunu tek bir kütüphane altında topladım. Kütüphaneyi wemos d1 mininin içine micro python firmware ile girdikten sonra `import AllInOne` demeniz ve sonrasında AllInOne.connect() ve AllInOne.send() fonksiyonlarını kullanmalısınız. Herhangi hata olduğunda geri dönüş alırsınız.

4. Micropython kullanmak için micropython dizininin içine bakmalısınız. Şuan da wemos d1 mini üzerinde sadece 1 tane analog okuyucu olduğu için analog çoğaltıcı veya mux bekliyoruz. Bu sayede 4'den fazla analog pinimiz olacak. Bu kısımdan sonrası sensörlerin maket üzerinde birleştirmek ve test etmek. Sonrasında veri madenciliği ile sınıflandırma verilerini kullanmayı planlıyorum. K yakınlık kümesi en mantıklısı gibi duruyor. 